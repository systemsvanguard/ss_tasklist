# Numeris Single Source Task List

Numeris Single Source Task List management application.  This is the front-end, built using Angular 12, Fontawesome icons, and Bootstrap v 5 CSS framework.  It consumes and gets data from a RESTful API backend using MS SQL.  It allows us to display, add, search, and manage Single Source Tasks.

## Numeris Task List Application :
- Each Task has id, title, tasktype, weight, description, published status.
- We can create, retrieve, update, delete Tasks.
- There is a Search bar for finding Tasks by title.



## Steps to Install
- Run the command below from the command line / terminal / command prompt.
- git clone https://systemsvanguard@bitbucket.org/systemsvanguard/ss_tasklist.git
- cd ss_tasklist\ 
- ensure your have Node & NPM pre-installed. Run commands 'node --version && npm -v'.
- npm install.  (This ensures all dependencies are installed).
- Ensure to rename local ".env4Display" to ".env".  
- Start the server with "ng serve --port 4200" 
- Runs on port 4200 --> http://localhost:4200/. 


## Features
- Angular 12 framework
- Bootstrap v5 CSS framework
- Angular Material UI
- FontAwesome 5 icons
- Google Fonts
- Filler text from https://pirateipsum.me
- dotenv & environment variables


## License
This project is licensed under the terms of the **MIT** license.


## Screenshot
![Screen 01](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen01.png) 


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
