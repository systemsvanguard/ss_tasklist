import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-etl28daysanalysis',
  templateUrl: './etl28daysanalysis.component.html',
  styleUrls: ['./etl28daysanalysis.component.css']
})
export class Etl28daysanalysisComponent implements OnInit {


  private current_date  = '2021-11-22';
  public etlstatus : any ; // find ETL job run status
  
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getETLJobStatus() 
  }

  getETLJobStatus() { 
    const apiETLStatus = `http://localhost:1339/api/engTasks/etl/${this.current_date}/`; 
    // test with http://localhost:1339/api/engTasks/etl/2021-11-21 
    this.http.get<any>(apiETLStatus).subscribe((res) => {
      this.etlstatus  =  res   
      // console.log(this.etlstatus)
      /* 
      we are only interested in reporting on the ETL jobs that did NOT retrun 'SUCCEEDED. Most/ all jobs will have a 'STARTED' status;we want the one's that did not suceed.    
      Using Angular pipes in the UI to filter, if API does not filter. 
      Find the job number codes from the 'ETLLogTypes' table.  Use to help you, the T-SQL query below.

        SELECT TOP 50  etl.ETLLogID , etl.Status , etl.Message, etl.ETLLogTypeID  
          , ett.LogTypeDescription , etl.LogDate 
        FROM PANEL_ADMIN_hunter.dbo.ETLLogs as etl  
          INNER JOIN PANEL_ADMIN_hunter.dbo.ETLLogTypes as ett  ON ett.ETLLogTypeID = etl.ETLLogTypeID 
        WHERE etl.LogDate >= '2021-11-21' AND etl.Status = 'SUCCEEDED' 
        ORDER BY ett.ETLLogTypeID ASC, etl.ETLLogID  DESC 

      */
    }  )
  }


}
