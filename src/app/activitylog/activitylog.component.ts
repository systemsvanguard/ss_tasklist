import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ManualTasksService } from '../services/manual.service';

@Component({
  selector: 'app-activitylog',
  templateUrl: './activitylog.component.html',
  styleUrls: ['./activitylog.component.css']
})
export class ActivitylogComponent implements OnInit {

  eng = JSON.parse(sessionStorage.getItem('currentUser'))[0].EngCode
  lastDate = 'Never'
  todayDate: any
  day = new Date();

  activityLog :any

  constructor(private msvc: ManualTasksService,public datePiper: DatePipe) { 

    this.todayDate =this.datePiper.transform(this.day, 'yyyy-MM-dd');
  }

  ngOnInit(): void {

    this.msvc.getActivityLog(this.eng,this.todayDate).subscribe(data =>{

      this. activityLog = data
    })
  }

}
