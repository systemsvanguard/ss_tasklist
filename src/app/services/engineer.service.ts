import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EngineerService {


  // service to return REST API data for Admin - Engineers CRUD call
  
  apiEngineer: string = 'http://localhost:1339/api/admin/engineers';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }
  
  // Handle API errors ----->
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'An issue with the back-end server occurred; please try again later.');
  };
  //------------------------>
  

  // Show lists of Engineers | CRUD - Retrieve all engineers
  getEngineers(): Observable<any> {
    return this.httpClient.get(this.apiEngineer).pipe(
      catchError(this.handleError)
    );
  }

  // Create new Engineer
  getEngineer(EngCode: any): Observable<any> {
    return this.httpClient.get(`${this.apiEngineer}/${EngCode}`).pipe(
      catchError(this.handleError)
    );
  }

  createEngineer(data: any): Observable<any> {
    return this.httpClient.post(this.apiEngineer, data).pipe(
      catchError(this.handleError)
    );
  }

  // Edit/ Update Engineer
  updateEngineer(EngCode: any, data: any): Observable<any> {
    return this.httpClient.put(`${this.apiEngineer}/${EngCode}`, data).pipe(
      catchError(this.handleError)
    );
  }

  // Delete Engineer
  deleteEngineer(EngCode: any): Observable<any> {
    return this.httpClient.delete(`${this.apiEngineer}/${EngCode}`).pipe(
      catchError(this.handleError)
    );
  }

  // Search By Engineer First Name
  filterByTitle(FirstName: any): Observable<any> {
    return this.httpClient.get(`${this.apiEngineer}?title_like=${FirstName}`).pipe(
      catchError(this.handleError)
    );
  }



}

/* data structure : [{"EngCode":"000","FirstName":"Eng","LastName":"Unassigned","Email":"default@default.address.com","Language":"en","IsAdmin":false,"LastLogin":null,"Active":false},   */
