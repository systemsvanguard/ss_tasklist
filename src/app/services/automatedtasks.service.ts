import { HttpClient,HttpHeaders, HttpParams, } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ManualInfo } from '../models/manualTasks';
import { ReportAnalysis } from '../models/reportAnalysis';
import { UserInfo } from '../models/userDetails';

@Injectable({
  providedIn: 'root'
})
export class AutomatedService {


  
  constructor(private http: HttpClient) { 

 
  }

getIncompleteTasks(engcode:string ,taskdate:string) {
    return this.http.get<any>(`${environment.apiurl}/engtasks/incomplete/${engcode}/${taskdate}`)
    .pipe(map(data => {
        
        return data;
    }) )

}

getCompleteTasks(engcode:string ,taskdate:string) {
    return this.http.get<any>(`${environment.apiurl}/engtasks/complete/${engcode}/${taskdate}`)
    .pipe(map(data => {
        
        return data;
    }) )

}
getFlaggedTasks(engcode:string ,taskdate:string) {
    return this.http.get<any>(`${environment.apiurl}/engtasks/flagged/${engcode}/${taskdate}`)
    .pipe(map(data => {
        
        return data;
    }) )

}

getDashboard(engcode:string ,taskdate:string ){
    
    return this.http.get<any>(`${environment.apiurl}/engtasks/engdashboard/${engcode}/${taskdate}`)
    .pipe(map(data => {
        
        return data[0];
    }) )

}

getHHTasks(engcode:string,taskdate:string,homenumber:number) {

    
    return this.http.get<any>(`${environment.apiurl}/household/${engcode}/${homenumber}/${taskdate}`)
    .pipe(map(data => {
        
        return data;
    }) )
}

getHHComments(engcode:string,taskdate:string,homenumber:number) {

    
    return this.http.get<any>(`${environment.apiurl}/household/getComments/${engcode}/${homenumber}/${taskdate}`)
    .pipe(map(data => {
        
        return data[0];
    }) )
}

updateHHComments(engcode:string,taskdate:string,hhinfo:any,comments:string) {

    return this.http.post<any>(`${environment.apiurl}/household/updateComments/${engcode}/${taskdate}`,{
        EngCode: engcode,
        Comments: comments,
        HomeNumber: hhinfo
        
      }).pipe(map(data=>{
        
        return data;
      }))
}


updateHHDetails(engcode:string,taskdate:string,hhinfo:any) {

    return this.http.post<any>(`${environment.apiurl}/household/updateTasks/${engcode}/${taskdate}`,{
        EngDailyTaskID: hhinfo.EngDailyTaskID,
        Completed: hhinfo.Completed
        
        
      }).pipe(map(data=>{
        
        return data;
      }))
}


updateHHFlagged(engcode:string,taskdate:string,hhinfo:any,flagged:any) {

    return this.http.post<any>(`${environment.apiurl}/household/updateFlag/${engcode}/${taskdate}`,{
        HouseholdFlag: flagged == true ? 0:1,
        EngCode: engcode,
        HomeNumber: hhinfo
        
        
      }).pipe(map(data=>{
        
        return data;
      }))
}

}
