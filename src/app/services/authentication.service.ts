import { HttpClient,HttpHeaders, HttpParams, } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserInfo } from '../models/userDetails';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<UserInfo>;
  public currentUser: Observable<UserInfo>;
  
  constructor(private http: HttpClient) { 

    this.currentUserSubject = new BehaviorSubject<UserInfo>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  login(username:string, password:string) {
    return this.http.post<[UserInfo]>(`${environment.apiurl}/user/login`, { EngCode:username, Password:password })
        .pipe(map(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            sessionStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user[0]);
            return user;
        }) )

        
}

logout() {
  // remove user from local storage and set current user to null
  sessionStorage.getItem('currentUser');
  sessionStorage.clear()
  this.currentUserSubject.next(null);
}

}
