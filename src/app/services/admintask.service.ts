//  Service class 
import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";  // enables http requests 
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})


export class AdmintaskService {

  constructor(private http : HttpClient) { }

  getAdmintasks() {
    return this.http.get('http://localhost:1339/api/admin/tasks');  
    // return this.http.get('https://jsonplaceholder.typicode.com/users');   // sample API for testing
  }

}

// example code from API | list of fields shown below
/*
    {
      "TaskID": 1,
      "TaskName": "New Recruit: Shipped, Not received (HH status 08 03)",
      "TaskType": "Automated task without dependency",
      "TaskTypeShort": "Automated",
      "Weight": 700,
      "TaskDescription": "Description of task 08 03.",
      "Active": true
    } 
*/
