import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Tasks } from "../models/tasks.model";
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class TasksService {
  

  // service to return REST API data for Admin - Tasks CRUD call
  // API path
  apiTasks = 'http://localhost:1339/api/admin/tasks';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  //---> HHTP CRUD methods -----> 
  // Create a new admin task 
  createItem(item): Observable<Tasks> {
    return this.http
      .post<Tasks>(this.apiTasks, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get single admin task data by ID
  getItem(TaskID): Observable<Tasks> {
    return this.http
      .get<Tasks>(this.apiTasks + '/' + TaskID)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get admin task data
  getList(): Observable<Tasks> {
    return this.http
      .get<Tasks>(this.apiTasks)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Update item by id
  updateItem(TaskID, item): Observable<Tasks> {
    return this.http
      .put<Tasks>(this.apiTasks + '/' + TaskID, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Delete item by id 
  deleteItem(TaskID) {
    return this.http
      .delete<Tasks>(this.apiTasks + '/' + TaskID, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  
  //---> handle API errors -----> 
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // client-side or network error occurred; resolve this.
      console.error('An client or network error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'An API related error occured.  Please try again later.');
  };  
  //---------------------------->
  
  

}
