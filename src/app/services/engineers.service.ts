import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Engineers } from "../models/engineers.model";
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EngineersService {


  // service to return REST API data for Admin - Engineers CRUD call
  // API path
  apiEngineers = 'http://localhost:1339/api/admin/engineers';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  //---> HHTP CRUD methods -----> 
  // Create a new engineer 
  createItem(item): Observable<Engineers> {
    return this.http
      .post<Engineers>(this.apiEngineers, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get single engineer data by ID
  getItem(EngCode): Observable<Engineers> {
    return this.http
      .get<Engineers>(this.apiEngineers + '/' + EngCode)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get admin task data
  getList(): Observable<Engineers> {
    return this.http
      .get<Engineers>(this.apiEngineers)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Update item by id
  updateItem(EngCode, item): Observable<Engineers> {
    return this.http
      .put<Engineers>(this.apiEngineers + '/' + EngCode, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Delete item by id 
  deleteItem(EngCode) {
    return this.http
      .delete<Engineers>(this.apiEngineers + '/' + EngCode, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  
  //---> handle API errors -----> 
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // client-side or network error occurred; resolve this.
      console.error('An client or network error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'An API related error occured.  Please try again later.');
  };  
  //---------------------------->
  
  

}
