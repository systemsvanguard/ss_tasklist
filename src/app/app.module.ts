import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { HttpClientModule,HTTP_INTERCEPTORS  } from '@angular/common/http';
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { NgxPaginationModule } from 'ngx-pagination'; 
import { DataTablesModule } from 'angular-datatables';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';

import { PreloadAllModules } from '@angular/router';
//Angular Material Components
// import { AppMaterialModule } from "./app.material-module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';

import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core'; 
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatBadgeModule } from "@angular/material/badge";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { ErrorIntercept } from './services/error.interceptor';
// import { SitefooterComponent } from "./shared/sitefooter/sitefooter.component";
// import { SitefooterComponent } from "src/app/shared/sitefooter/sitefooter.component";
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { WeightsComponent } from './admin/weights/weights.component';
import { DelegatesComponent } from './admin/delegates/delegates.component';
import { SettingsComponent } from './admin/settings/settings.component';
import { LongweekendsComponent } from './admin/longweekends/longweekends.component';
import { EngineerlistComponent } from './admin/engineer/engineerlist/engineerlist.component';
import { EngineerdetailComponent } from './admin/engineer/engineerdetail/engineerdetail.component';
import { EngineercreateComponent } from './admin/engineer/engineercreate/engineercreate.component';
import { TasklistComponent } from './admin/tasks/tasklist/tasklist.component';
import { TaskdetailComponent } from './admin/tasks/taskdetail/taskdetail.component';
import { TaskcreateComponent } from './admin/tasks/taskcreate/taskcreate.component';
import { AdminmanualtasksComponent } from './admin/tasks/adminmanualtasks/adminmanualtasks.component';
import { EngineerslistComponent } from './admin/engineers/engineerslist/engineerslist.component';
import { EngineerseditComponent } from './admin/engineers/engineersedit/engineersedit.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    // SitefooterComponent , 
    DashboardComponent,
    WeightsComponent,
    DelegatesComponent,
    SettingsComponent,
    LongweekendsComponent,
    EngineerlistComponent,
    EngineerdetailComponent,
    EngineercreateComponent,
    TasklistComponent,
    TaskdetailComponent,
    TaskcreateComponent,
    AdminmanualtasksComponent,
    EngineerslistComponent,
    EngineerseditComponent    
  ],
  imports: [
    
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    FormsModule,
    HttpClientModule,  
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxPaginationModule, 
    Ng2SearchPipeModule , 
    FlexLayoutModule ,     
    
    // AppMaterialModule, 
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,    
    MatCardModule, 
    MatStepperModule,
    MatBadgeModule,     
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,  
    DataTablesModule , 
    Ng2GoogleChartsModule, 
    
  ],
  exports: [
    MatFormFieldModule, 
     MatInputModule 
  ],
    
    
  providers: [{provide: HTTP_INTERCEPTORS,
    useClass: ErrorIntercept,
    multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
