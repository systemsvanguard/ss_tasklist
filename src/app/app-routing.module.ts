import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
// import pages
import { DashboardComponent } from "./admin/dashboard/dashboard.component";
import { DelegatesComponent } from "./admin/delegates/delegates.component";
import { LongweekendsComponent } from "./admin/longweekends/longweekends.component";
import { SettingsComponent } from "./admin/settings/settings.component";
import { WeightsComponent } from "./admin/weights/weights.component";
import { TasklistComponent } from "./admin/tasks/tasklist/tasklist.component";
import { TaskdetailComponent } from "./admin/tasks/taskdetail/taskdetail.component";
import { TaskcreateComponent } from "./admin/tasks/taskcreate/taskcreate.component";
import { EngineerseditComponent } from "./admin/engineers/engineersedit/engineersedit.component";

import { EngineerslistComponent } from "./admin/engineers/engineerslist/engineerslist.component";
import { EngineerlistComponent } from "./admin/engineer/engineerlist/engineerlist.component";
import { EngineerdetailComponent } from "./admin/engineer/engineerdetail/engineerdetail.component";
import { EngineercreateComponent } from "./admin/engineer/engineercreate/engineercreate.component";

const routes: Routes = [
{path:'',component: LoginComponent,data:{animation: 'login'}},
{path: 'dailytasks', loadChildren: () => import('./dailytaskslist/dailytaskslist.module').then(m => m.DailytaskslistModule) },
{path: 'dashboard', component: DashboardComponent }, 
{path: 'task', component: TasklistComponent }, 
{path: 'task/:TaskID', component: TaskdetailComponent }, 
{path: 'taskadd', component: TaskcreateComponent }, 
{path: 'delegates', component: DelegatesComponent }, 
{path: 'engineersedit', component: EngineerseditComponent }, 
{path: 'longweekends', component: LongweekendsComponent }, 
{path: 'weights', component: WeightsComponent }, 
{path: 'admintask', component: TasklistComponent }, 
{path: 'admintasks', component: TasklistComponent }, 
{path: 'settings', component: SettingsComponent }, 
//------------------>
// we will probably NOT need these pages diretly below IF we proceed with 'engineersedit' page
{path: 'engineers', component: EngineerslistComponent }, 
{path: 'engineer', component: EngineerlistComponent }, 
{path: 'engineer/:EngCode', component: EngineerdetailComponent } , 
{path: 'engineercreate', component: EngineercreateComponent } , 
//------------------>


{path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{

    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

