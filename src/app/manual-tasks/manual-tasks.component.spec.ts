import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualTasksComponent } from './manual-tasks.component';

describe('ManualTasksComponent', () => {
  let component: ManualTasksComponent;
  let fixture: ComponentFixture<ManualTasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManualTasksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
