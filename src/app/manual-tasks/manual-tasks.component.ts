import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ManualInfo } from '../models/manualTasks';
import { ReportAnalysis } from '../models/reportAnalysis';
import { ManualTasksService } from '../services/manual.service';
import {formatDate} from '@angular/common'; 

@Component({
  selector: 'app-manual-tasks',
  templateUrl: './manual-tasks.component.html',
  styleUrls: ['./manual-tasks.component.css']
})

export class ManualTasksComponent implements OnInit {

  // API examples : Manual Tasks | http://localhost:1339/api/manual/tasks/ama/20211207 
  displayedColumns: string[] = ['TaskName',  'TotalItems','CompletedItems', 'TaskLeft'];
  dataSource :any; lastDate = 'Never';comment ='';
  eng = JSON.parse(sessionStorage.getItem('currentUser'))[0].EngCode 

  // dateForAPI  : any = formatDate(Date.now() -  ( 1000*60*60*24  ), 'yyyy-MM-dd', 'en');  // this get's yesterday's date 
  dateForAPI : any = '2021-12-10' ;  
  // this page's API noes NOT needs a specific date, the latest available Task Date, to run; will run daily.  Example 2021-12/09

  report: ReportAnalysis
  todayDate: any
  day = new Date();
  


   constructor(private manualsvc: ManualTasksService,private formBuilder: FormBuilder,public datePiper: DatePipe) {

this.todayDate =this.datePiper.transform(this.day, 'yyyy-MM-dd');


    }

  ngOnInit(): void {

    this.manualsvc.getManualTasks(this.eng, this.dateForAPI ).subscribe(data=>{  
    // this.manualsvc.getManualTasks(this.eng,'2021-12-10').subscribe(data=>{  

     this.dataSource = data;
      
      
    })


    this.manualsvc.getReport(this.eng,'2023-07-26').subscribe(data=> {
      this.report = data;

      
    })

    this.manualsvc.getComments(this.eng,'2017-07-26').subscribe(data=>{
      this.comment = data;
    })

    this.manualsvc.getResources(this.eng).subscribe(data=> {

     
  this.lastDate = data.Last28DaysAnalysis ==null ? 'Never': data.Last28DaysAnalysis.toString().split('T')[0];
      
    })
  }

  updateTasks(item: any) {

    this.manualsvc.UpdateCount(item,this.eng)
    .subscribe({
      next: (data:any) => {
       
      },
      error: (e:any) => console.error(e),
      complete: () => {} 
  })
}


updateReport(flag:boolean ,type :string) {
  
  let item : ReportAnalysis = {
EngDailyReportAnalysisID: this.report == undefined ? 0 : this.report.EngDailyReportAnalysisID,
NeedsAnalysis: type == 'analysis' ? flag : this.report ==undefined ? false : this.report.NeedsAnalysis,
Completed : type == 'completed' ? flag : this.report ==undefined ? false : this.report.Completed
  }

  this.manualsvc.updateReport(item,this.eng,'2023-07-26')   .subscribe({
    next: (data:any) => {
      this.lastDate = data;
    },
    error: (e:any) => console.error(e),
    complete: () => console.info('complete') 
})
}


updateComment() {
  
  this.manualsvc.updateComment(this.eng,'2017-07-26',this.comment)   .subscribe({
    next: (data:any) => {
      
    },
    error: (e:any) => console.error(e),
    complete: () => console.info('complete') 
})
}






}
