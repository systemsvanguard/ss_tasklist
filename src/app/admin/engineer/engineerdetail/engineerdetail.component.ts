import { Component, OnInit } from '@angular/core';
import { EngineerService } from "src/app/services/engineer.service";  // RestApiService
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-engineerdetail',
  templateUrl: './engineerdetail.component.html',
  styleUrls: ['./engineerdetail.component.css']
})
export class EngineerdetailComponent implements OnInit {

  pageTitle : string; 
  id = this.actRoute.snapshot.params['EngCode'];
  engineer: any = {};   

  constructor(
    public restApi: EngineerService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { 
  }

  ngOnInit(): void {
    this.pageTitle = "Edit Specific Engineer";

    // this.engineerService.getEngineer(this.EngCode).subscribe((data: {}) => {
    //   this.engineer = data;
    // })
  }

  // Update employee data
  updateEngineer() {
    // if(window.confirm('Are you sure, you want to update?')){
    //   this.engineerService.updateEngineer(this.EngCode, this.engineer).subscribe(data => {
    //     this.router.navigate(['/engineer'])
    //   })
    // }
  }

}
