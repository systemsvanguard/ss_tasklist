import { Component, OnInit } from '@angular/core';
import { EngineerService } from "src/app/services/engineer.service";

@Component({
  selector: 'app-engineerlist',
  templateUrl: './engineerlist.component.html',
  styleUrls: ['./engineerlist.component.css']
})

export class EngineerlistComponent implements OnInit {

  Engineer: any = [];
  pageTitle : string;
  p: number = 1; 
  searchterm = '';
  // pagination library: https://www.npmjs.com/package/ngx-pagination
  // search library: https://www.npmjs.com/package/ng2-search-filter

  constructor(
    public engineerService: EngineerService  
  ) { }

  ngOnInit() {
    this.loadEngineers();
    this.pageTitle = "List of Engineers";
  }

  // Get engineer list
  loadEngineers() {
    return this.engineerService.getEngineers().subscribe((data: {}) => {
      this.Engineer = data;
    })
  }

  // Delete engineer
  deleteEngineer(EngCode : any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.engineerService.deleteEngineer(EngCode).subscribe(data => {
        this.loadEngineers()
      })
    }
  }  

}

