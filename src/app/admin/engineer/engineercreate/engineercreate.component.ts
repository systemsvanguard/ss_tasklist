import { Component, OnInit } from '@angular/core';
import { EngineerService } from "src/app/services/engineer.service";

@Component({
  selector: 'app-engineercreate',
  templateUrl: './engineercreate.component.html',
  styleUrls: ['./engineercreate.component.css']
})
export class EngineercreateComponent implements OnInit {

  pageTitle : string;
  engineer = {
    EngCode: '',
    FirstName: '', 
    LastName: '', 
    Email: '', 
    Language: '', 
    IsAdmin: '', 
    LastLogin: '',
    Active: ''
  };
  isEngineerAdded = false;

  constructor(private engineerService: EngineerService) { }

  ngOnInit(): void { 
    this.pageTitle = "Create Engineer ";  
  }

  // Add New
  addEngineer(): void {
    const data = {
      EngCode: this.engineer.EngCode,
      FirstName: this.engineer.FirstName,
      LastName: this.engineer.LastName,
      Email: this.engineer.Email,
      Language: this.engineer.Language,
      IsAdmin: this.engineer.IsAdmin,
      LastLogin: this.engineer.LastLogin,
      Active: this.engineer.Active 
    };
    if (!data.FirstName) {
      alert('Please add First Name.');
      return;
    }

    this.engineerService.createEngineer(data)
      .subscribe(
        response => {
          console.log(response);
          this.isEngineerAdded = true;
        },
        error => {
          console.log(error);
        });
  }

  // Reset on adding new
  newEngineer(): void {
    this.isEngineerAdded = false;
    this.engineer = {
      EngCode: '', 
      FirstName: '', 
      LastName: '', 
      Email: '', 
      Language: '', 
      IsAdmin: '', 
      LastLogin: '',
      Active: ''      
    };
  }




}
