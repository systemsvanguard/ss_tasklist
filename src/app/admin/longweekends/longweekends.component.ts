import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-longweekends',
  templateUrl: './longweekends.component.html',
  styleUrls: ['./longweekends.component.css'],
})
export class LongweekendsComponent implements OnInit {
  pageTitle: string;
  p: number = 1;
  searchterm = '';
  // pagination library: https://www.npmjs.com/package/ngx-pagination
  // search library: https://www.npmjs.com/package/ng2-search-filter

  dateRange = new FormGroup({
    startDate: new FormControl(),
    endDate: new FormControl(),
  });

  apiStartDate = this.dateRange.value.startDate || '20181210';
  apiEndDate = this.dateRange.value.endDate || '20211210';
  // apiStartDate  =  this.dateRange.value.startDate ;
  // apiEndDate    =  this.dateRange.value.endDate ;
  // apiStartDate  =  '20190820' ;
  // apiEndDate    =  '20200910' ;

  public adminLongWeekendReminders: any;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    // this.getLongWeekendReminders() ;
    this.getLongWeekendUnfiltered();
    this.pageTitle = 'Long Weekend Reminders';
  }

  getLongWeekendReminders() {
    // Loads data on initial page load.  NOT yet filetered by Date Range Picker
    this.dateRange.value.startDate;
    this.dateRange.value.endDate;

    const apiLongWeekendReminders = `http://localhost:1339/api/admin/longweekReminder/${this.apiStartDate}/${this.apiEndDate}/`;
    // const apiLongWeekendReminders = 'http://localhost:1339/api/admin/longweekReminder/20181210/20211210/';  // example 20191210 - 20210110

    this.http.get<any>(apiLongWeekendReminders).subscribe((res) => {
      this.adminLongWeekendReminders = res;
      // console.log(this.adminLongWeekendReminders)
    });
  }

  getLongWeekendUnfiltered() {
    // Loads data on initial page load.  NOT yet filetered by Date Range Picker
    const apiLongWeekendReminders = `http://localhost:1339/api/admin/longweekReminder/${this.apiStartDate}/${this.apiEndDate}/`;
    // const apiLongWeekendReminders = 'http://localhost:1339/api/admin/longweekReminder/20181210/20211210/';  // OR example 20191210 - 20210110

    this.http.get<any>(apiLongWeekendReminders).subscribe((res) => {
      this.adminLongWeekendReminders = res;
      // console.log(this.adminLongWeekendReminders)
    });
  }

  filterLongWeekendReminders() {
    // used to test button onClick data load | temporary
    const apiLongWeekendReminders = `http://localhost:1339/api/admin/longweekReminder/${this.apiStartDate}/${this.apiEndDate}/`;
    // const apiLongWeekendReminders = 'http://localhost:1339/api/admin/longweekReminder/20190820/20200910/';  // example 20191210 - 20210110

    this.http.get<any>(apiLongWeekendReminders).subscribe((res) => {
      this.adminLongWeekendReminders = res;
      // console.log(this.adminLongWeekendReminders)
    });
  }
}
