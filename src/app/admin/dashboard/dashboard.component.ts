import { Component, OnInit } from '@angular/core';
import { GoogleChartInterface, GoogleChartType } from 'ng2-google-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  pageTitle : string;
  
  constructor() { }

  ngOnInit(): void {
    this.pageTitle = "Administration Dashboard";
  }
  //---- Graph - All Engineers ------->   barChartAllEngineers
  public barChartAllEngineers: GoogleChartInterface = {
    chartType: GoogleChartType.BarChart,
    options: {
      // 'width': 400, 
      'height': 150, 
      'title': 'Tasks Completed' 
    },
    dataTable: [
      ['Task', 'Status'],
      ['All Engineers', 4900 ]
    ],
    //firstRowIsData: true,
  };
  
  //---- Graph - By Engineer ------->
  public barChartByEngineer: GoogleChartInterface = {
    chartType: GoogleChartType.BarChart,
    options: {      
      // 'width': 400, 
      'height': 700, 
      'title': 'Task Completion' 
    },
    dataTable: [
      ['Task', 'Status'],
      ['Aldon Chiang (ALD)', 1000 ], 
      ['Alexis Castillo (LEX)', 1860 ], 
      ['Andréanne Lamothe (ATL)', 2320 ], 
      ['Apoorve Sharma (ASH)', 2160 ], 
      ['Blanca Hernandez (BHE)', 1800 ], 
      ['Christopher Upshaw (CUP)', 2890 ], 
      ['Eduardo Mottin (EMO)', 1150 ], 
      ['Errol Lobo (LOB)', 920 ], 
      ['Eve Isaac (EVE)', 1400 ], 
      ['Fanny Aubin (FAA)', 2980 ], 
      ['Fanny Hagerimana (FHA)', 3560 ], 
      ['Harshen Ramsurn (HRM)', 700 ], 
      ['James Coughlin (JCO)', 2150 ], 
      ['Jennifer Opena (JOP)', 1690 ], 
      ['Jovita Del Rosario (JDR)', 1740], 
      ['Juliette Hales (JHA)', 1000], 
      ['Kath Mong (KMO)', 1650 ], 
      ['Manouchka Elinor (MER)', 2430 ], 
      ['Mariette Kanga (MKA)', 3390 ], 
      ['Nancy Meng (NAN)', 1100 ], 
      ['Nobue Karen Yoshioka (NOY)', 2130 ], 
      ['Nubia Pabon (NUB)', 1630 ], 
      ['Phillip Barros Cunha (PBC)', 1690 ], 
      ['Raajev Chand (RCH)', 1680 ], 
      ['Robert De Lery (RDL)', 3150 ], 
      ['Salina Leung (SLE)', 1670 ], 
      ['Stephane Mateus (SMA)', 1260 ], 
      ['Suzette Palmere (SPA)', 1800 ], 
      ['Tammy Brown (TMB)', 1300 ], 
      ['Tarunveer Singh (TSI)', 1600 ], 
      ['Vicky Barrette (VBA)', 1400 ]
    ],
    //firstRowIsData: true,
  };
  //------------------>

}
