// engineersedit.component.ts
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EngineersService } from 'src/app/services/engineers.service';
import * as _ from 'lodash';
import { NgForm } from '@angular/forms';
import { Engineers } from 'src/app/models/engineers.model';

@Component({
  selector: 'app-engineersedit',
  templateUrl: './engineersedit.component.html',
  styleUrls: ['./engineersedit.component.css'],
})
export class EngineerseditComponent implements OnInit {
  pageTitle: string;

  @ViewChild('engineersForm', { static: false })
  engineersForm: NgForm;

  engineersData: Engineers;

  dataSource = new MatTableDataSource();

  // data columns: EngCode, FirstName, LastName, Email, Language, IsAdmin, LastLogin, Active
  displayedColumns: string[] = [
    'EngCode',
    'FirstName',
    'LastName',
    'Email',
    'Language',
    'IsAdmin',
    'LastLogin',
    'Active',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  isEditMode = false;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private httpDataService: EngineersService) {
    this.engineersData = {} as Engineers;
  }

  ngOnInit(): void {
    this.pageTitle = 'List of Engineers';
    // Initializing Datatable pagination
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    // Fetch All records on Page load
    this.getAllItems();
  }
  //-------------------------------------------------->

  // -- CRUD -- GET | retrieve all items/ records ---->
  getAllItems() {
    this.httpDataService.getList().subscribe((response: any) => {
      this.dataSource.data = response;
    });
  }

  editItem(element) {
    this.engineersData = _.cloneDeep(element);
    this.isEditMode = true;
  }

  cancelEdit() {
    this.isEditMode = false;
    this.engineersForm.resetForm();
  }

  // -- CRUD -- CREATE | create a new record ---->
  /* 
  createItem() {
    this.httpDataService.createItem(this.engineersData).subscribe((response: any) => {
      this.dataSource.data.push({ ...response })
      this.dataSource.data = this.dataSource.data.map(o => {
        return ;
        // return o;
      })
    });
  }
  */

  // ================================================

  // -- CRUD -- U | UPDATE / edit items/ records ---->
  /*
  updateItem() {
    this.httpDataService.updateItem(this.engineersData.EngCode, this.engineersData).subscribe((response: any) => {

      // Approach #1 to update datatable data on local itself without fetching new data from server
      this.dataSource.data = this.dataSource.data.map((o: Engineers) => {
        if (o.EngCode === response.EngCode) {  
          o = response;
        }
        return o;
        // return o;
      })

      // Approach #2 to re-call getAllItems() to fetch updated data
      // this.getAllItems()

      this.cancelEdit()

    });
  }
  */

  // -- CRUD -- D | DELETE / remove items/ records ---->
  deleteItem(EngCode) {
    this.httpDataService.deleteItem(EngCode).subscribe((response: any) => {
      // Approach #1 to update datatable data on local itself without fetching new data from server
      /*
      this.dataSource.data = this.dataSource.data.filter((o: Engineers) => {
        return o.EngCode !== EngCode ? o : false;  // id == EngCode 
      })
      */

      console.log(this.dataSource.data);

      // Approach #2 to re-call getAllItems() to fetch updated data
      // this.getAllItems()
    });
  }
  //------------------------>

  onSubmit() {
    if (this.engineersForm.form.valid) {
      if (this.isEditMode)
        // this.updateItem()
        console.log('Update Item');
      // this.createItem();
      else console.log('Create Item');
    } else {
      console.log('Please enter valid data!');
    }
  }

  //------------------------------>
}
