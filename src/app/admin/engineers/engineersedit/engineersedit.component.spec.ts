import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineerseditComponent } from './engineersedit.component';

describe('EngineerseditComponent', () => {
  let component: EngineerseditComponent;
  let fixture: ComponentFixture<EngineerseditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineerseditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineerseditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
