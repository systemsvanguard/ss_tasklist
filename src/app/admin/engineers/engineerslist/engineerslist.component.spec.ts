import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineerslistComponent } from './engineerslist.component';

describe('EngineerslistComponent', () => {
  let component: EngineerslistComponent;
  let fixture: ComponentFixture<EngineerslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineerslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineerslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
