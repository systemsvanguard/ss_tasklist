import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormGroup, FormControl} from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-engineerslist',
  templateUrl: './engineerslist.component.html',
  styleUrls: ['./engineerslist.component.css']
})
export class EngineerslistComponent implements OnInit {

  pageTitle : string;
  p: number = 1; 
  searchterm = '';
  // pagination library: https://www.npmjs.com/package/ngx-pagination
  // search library: https://www.npmjs.com/package/ng2-search-filter

  public  adminEngineersList : any ;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getEngineersList(); 
    this.pageTitle = "List of Engineers";
  }


  getEngineersList() {  
    // get list of Engineers from API
    const    adminEngineersList = 'http://localhost:1339/api/admin/engineers';   

    this.http.get<any>(adminEngineersList).subscribe((res) => {
      this.adminEngineersList  =  res   
      // console.log(this.adminEngineersList)
    }  )
  }

}

