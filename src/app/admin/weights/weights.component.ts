import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
declare var $ :any;

@Component({
  selector: 'app-weights',
  templateUrl: './weights.component.html',
  styleUrls: ['./weights.component.css']
})


export class WeightsComponent implements OnInit {  

  /*
  constructor() { }
  ngOnInit(): void {
  }
  */
  dynamicdata : any;
  constructor(private http: HttpClient){
    //get request from web api
    this.http.get('http://localhost:1339/api/admin/userweights/20181210/20211210/').subscribe(data => {
    
      this.dynamicdata = data;
        setTimeout(()=>{   
          $('#dataTables-example').DataTable( {
            pagingType: 'full_numbers',
            pageLength:  25,
            processing: true,
            lengthMenu : [5, 10, 20, 25, 50, 100]
        } );
        }, 1);
          }, error => console.error(error));
  }

   
     ngOnInit() {
      
      //datepicker
      $('.dateadded').on( 'change', function (ret :any) {
 
        var v = ret.target.value  // getting search input value
        
        $('#dataTables-example').DataTable().columns(5).search(v).draw(); // number of columns displyed
    } );
    }

}
