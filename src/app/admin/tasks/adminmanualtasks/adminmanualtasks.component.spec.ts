import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminmanualtasksComponent } from './adminmanualtasks.component';

describe('AdminmanualtasksComponent', () => {
  let component: AdminmanualtasksComponent;
  let fixture: ComponentFixture<AdminmanualtasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminmanualtasksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminmanualtasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
