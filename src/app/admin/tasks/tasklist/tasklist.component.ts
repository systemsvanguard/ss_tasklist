import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdmintaskService } from "src/app/services/admintask.service";      // service for API  


@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent implements OnInit {

  pageTitle : string;
  // columnsToDisplay = ['TaskID', 'Weight',  'TaskName', 'TaskType', 'TaskTypeShort', 'TaskDescription', 'Active' ]; 
  columnsToDisplay = [ 'TaskID',  'Weight',  'TaskName','TaskDescription', 'Active',  'TaskTypeShort' ];  // took OUT 'TaskType' 
  
  dataSource!:MatTableDataSource<any> ;   // source for our data 
  @ViewChild('paginator') paginator! : MatPaginator; 
  @ViewChild(MatSort) matSort! : MatSort;
  
  constructor(private admintaskService: AdmintaskService)  { }

  ngOnInit() {
    this.pageTitle = "Tasks";
    this.admintaskService.getAdmintasks().subscribe((response:any) =>{
      this.dataSource = new MatTableDataSource(response  ); 
      /* there are 2 nodes on this API end-point; 'automated' & 'manual' | I have selected just 'automated' */
      /* see line # 43 of 'admin.service.ts' in API | line changed from "return result;" TO "return result.automated;"  */
      this.dataSource.paginator = this.paginator ;
      this.dataSource.sort = this.matSort;
      console.log(this.dataSource.data );
    })

  }

  filterData($event : any){
    this.dataSource.filter = $event.target.value;
  }
  //------------------------->

}
