import { Component } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { slideInAnimation } from './animations';
import { UserInfo } from './models/userDetails';
import { AuthenticationService } from './services/authentication.service';
import {formatDate} from '@angular/common'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [slideInAnimation]
})
export class AppComponent {
  
  currentYear = new Date().getFullYear();
  
  currentUser: UserInfo;
  // currentDateYesterday  : string = "2021-12-10"   
  currentDateYesterday  : any = formatDate(Date.now() -  ( 1000*60*60*24  ), 'yyyy-MM-dd', 'en');
  
  
  constructor(private router: Router,private authenticationService: AuthenticationService){

    
    this.authenticationService.currentUser.subscribe(x => {
      this.currentUser = x;
      
    
    });
  }

  getAnimationData(outlet: RouterOutlet) {
    return outlet?.activatedRouteData?.['animation'];
  }

  logout() {

    this.authenticationService.logout();
    this.router.navigate(['/'])
  }


}
