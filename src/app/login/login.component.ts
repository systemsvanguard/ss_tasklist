import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginFormBuilder = this.formBuilder.group({

    engineer: ['',Validators.required],
    password: ['']
  })
 
  



  constructor(private formBuilder: FormBuilder,private authenticationService: AuthenticationService ,private router: Router) { 

  }

  ngOnInit(): void {
    
  }

  onSubmit() {
    
    const loginControls = this.loginFormBuilder.controls;
    this.authenticationService.login(loginControls['engineer'].value,loginControls['password'].value)
    .subscribe({
      next: (data) => {
        this.router.navigate(['/dailytasks']);
      },
      error: (e) => console.error(e),
      complete: () => console.info('complete') 
  })
    

  }

}
