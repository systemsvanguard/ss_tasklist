export class Tasks {
    TaskID: number;
    TaskName: string;
    TaskType: string;
    TaskTypeShort: string;
    Weight: number;
    TaskDescription: string;
    Active: boolean;
}
