export class Dashboard {

     CompletedByHH:number;
     CompletedByWeight:number;
     FlaggedHH:number;
     InProgressHH:number;
     InProgressByWeight:number;
     UnstartedByHH:number;
     UnstartedByWeight:number
}