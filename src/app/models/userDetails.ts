export class UserInfo {

    Active: boolean 
    CreatedBy: string;
    CreatedOn: string;
    Deleted: boolean;
    Email: string;
    EngCode:string;
    FirstName: string;
    IsAdmin: boolean;
    Language: string;
    LastLogin: string;
    LastName: string;
    UpdatedBy: string;
    UpdatedOn: string;


}