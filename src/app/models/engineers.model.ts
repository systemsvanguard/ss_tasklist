export class Engineers {
    EngCode: string;
    FirstName: string;
    LastName: string;
    Email: string;
    Language: string;
    IsAdmin: boolean;
    LastLogin: Date;  // Date class 
    // LastLogin: string;  
    Active: boolean;
}
