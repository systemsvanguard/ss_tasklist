import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserInfo } from '../models/userDetails';
import { AuthenticationService } from '../services/authentication.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { ChartType } from 'chart.js';
import { ManualTasksService } from '../services/manual.service';
import { DatePipe } from '@angular/common';
import { AutomatedService } from '../services/automatedtasks.service';
import { Dashboard } from '../models/dashboard';
import { SingleDataSet } from 'ng2-charts';
import { single } from 'rxjs';
import { formatDate} from '@angular/common'; 


@Component({
  selector: 'app-dailytaskslist',
  templateUrl: './dailytaskslist.component.html',
  styleUrls: ['./dailytaskslist.component.css']
})
export class DailytaskslistComponent implements OnInit {
  // This page used for Bar Graph
  // API examples : Bar Graph | http://localhost:1339/api/engtasks/engdashboard/ama/20211210  
  // API examples : Left side - HH by PA | http://localhost:1339/api/engtasks/incomplete/ama/20211210    
  // API examples : Right  side - Tasks by HH | http://localhost:1339/api/household/ama/7090248/2021-12-10 

  eng = JSON.parse(sessionStorage.getItem('currentUser'))[0].EngCode 
  // dateForAPI  : any = formatDate(Date.now() -  ( 1000*60*60*24  ), 'yyyy-MM-dd', 'en'); // used in 1 place below  // get's yesterday's date
  dateForAPI : any = '2021-12-10' ;  // this page's API needs a specific date, the latest available Task Date, to run. Example 2021-12/07 

  todayDate: any; day = new Date();
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ['ByHH','ByWeight']
  //['CompletedByHH', 'CompletedByWeight', 'InProgressHH', 'InProgressByWeight', 'UnstartedByHH','UnstartedByWeight'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins: any = [];

  barChartData: ChartDataSets[] = [];
  chartColors = [
    {
      backgroundColor: 'green'
    },
    {
      backgroundColor: 'orange',
    }
    ,
    {
      backgroundColor: 'red',
    }
  ];

  constructor(private mvsc : AutomatedService,public datePiper: DatePipe){
    this.todayDate =this.datePiper.transform(this.day, 'yyyy-MM-dd');

  }

  ngOnInit(): void {

    this.mvsc.getDashboard(this.eng, this.dateForAPI).subscribe(datas => {  
    // this.mvsc.getDashboard(this.eng,'2021-12-10').subscribe(datas => {
      console.log(this.dateForAPI);


     this.barChartData = [
     
      { data: [datas.CompletedByHH,datas.CompletedByWeight ],   label: 'Completed' },
      { data: [datas.InProgressByHH ,datas.InProgressByWeight], label : 'InProgress'},
      { data: [datas.UnstartedByHH,datas.UnstartedByWeight],    label : 'Unstarted'},
   
      ] })
  }

  addChartData(datas:any) {
    this.barChartData = [
     
       { data: [datas.CompletedByHH,datas.CompletedByWeight ],   label: 'Completed' },
       { data: [datas.InProgressByHH ,datas.InProgressByWeight], label : 'InProgress'},
       { data: [datas.UnstartedByHH,datas.UnstartedByWeight],    label : 'Unstarted'}, 
    
       ] }

}
