import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailytaskslistComponent } from './dailytaskslist.component';

describe('DailytaskslistComponent', () => {
  let component: DailytaskslistComponent;
  let fixture: ComponentFixture<DailytaskslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailytaskslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailytaskslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
